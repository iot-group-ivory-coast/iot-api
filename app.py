# back.py
# (c) 2019 fieldcloud SAS. all rights reserved
# VERSION:
# DATE: 14/10/2020
# AUTHOR: mds@fieldcloud.com,mnayebare@fieldcloud.com
# back-end api file for the website

import os
from flask import Flask
from flask import Flask,request, json, jsonify
from flask_cors import CORS
import requests
import smtplib, ssl
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


from twilio.rest import Client


app = Flask(__name__)
CORS(app)

@app.route("/", methods=['GET'])
def index():
    return("ivory coast iot-training api")

# #send sms
# @app.route("/sms", methods=['POST'])
# def post_data_from_provider():
#     data = request.get_data().decode("utf-8").split()
#     mobile = data[0]
#     temp = data[1]

#     account_sid = os.environ.get('AC_SID')
#     auth_token = os.environ.get('AUTH_KEY')
#     client = Client(account_sid, auth_token)

#     message = client.messages \
#                     .create(
#                         body="La température a atteint le bon seuil de "  +  str(temp)   +  " degrés",
#                         from_='+15005550006',
#                         to=str(mobile)
#                     )
#     return(jsonify({'Success':'message received'}), 200)




#send email
@app.route("/email", methods=['POST'])
def sendEmail():
    #data = request.get_data().decode("utf-8").split()
    data = request.get_json()
    print(data)
    if (data.get('signal') == '1' or data.get('signal') == '100'):
        temp = data.get('payload')
        email = data.get('email')
        print('sending email to {} with temperature value {}'.format(email,temp))
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login("iotivorycoast@gmail.com", "p@#!iVvfNg3pTRh4ts")
        msg = MIMEMultipart()
        msg['From']="iotivorycoast@gmail.com"
        msg['To']=email
        msg['Subject']="Alerte de capteur {}".format(data.get('deviceName'))
        message= "Depuis le objet {} j'ai le plaisir de vous informer que la température est {} degrés Celsius".format(data.get('deviceName'),str(temp))
        msg.attach(MIMEText(message, 'plain'))
        if s.send_message(msg) is None:
             return (jsonify({'Error': 'Error occured in sending message'}), 400)
        else:
            return (jsonify({'Success':'email sent'}), 200)
        s.quit()
    else:
        print('doing nothing')
        return (jsonify({'Error':'Unauthorized device'}), 401)
    return (jsonify({'Bad Request': 'unique client not found or key does not match client'}), 400)

if __name__ == '__main__':
    app.run(port='3000', host='0.0.0.0', debug=True)
